package com.example.assignment;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
class TicketControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private TestHelper testHelper;

    @Test
    @Order(1)
    public void getTicketsTest() throws Exception {
        mockMvc.perform(get("/tickets"))
                .andDo(print())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json("[]"))
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    @Order(2)
    public void createTicketTest() throws Exception {
        for (String content : testHelper.loadResources("classpath:createTicket_invalid*.json")) {
            mockMvc.perform(post("/tickets").content(content).contentType(MediaType.APPLICATION_JSON))
                    .andDo(print())
                    .andExpect(status().is4xxClientError());
        }

        String content = testHelper.loadResource("classpath:createTicket_valid.json");
        mockMvc.perform(post("/tickets").content(content).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is4xxClientError());

        content = testHelper.loadResource("classpath:addCustomer_valid_1.json");
        mockMvc.perform(post("/customers").content(content).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is2xxSuccessful());

        content = testHelper.loadResource("classpath:createTicket_valid.json");
        mockMvc.perform(post("/tickets").content(content).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("id").value(1))
                .andExpect(jsonPath("assignee").isEmpty())
                .andExpect(jsonPath("title").value("test title"))
                .andExpect(jsonPath("description").value("test description"))
                .andExpect(jsonPath("customer.id").value(1))
                .andExpect(jsonPath("customer.name").value("test AB"))
                .andExpect(jsonPath("status").value("IN_QUEUE"))
                .andExpect(jsonPath("comments").isEmpty());
    }

    @Test
    @Order(3)
    public void getTicketTest() throws Exception {
        mockMvc.perform(get("/tickets/0"))
                .andDo(print())
                .andExpect(status().is4xxClientError());

        mockMvc.perform(get("/tickets/1"))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    @Order(4)
    public void updateTicketTest() throws Exception {
        for (String content : testHelper.loadResources("classpath:updateTicket_invalid*.json")) {
            mockMvc.perform(patch("/tickets/0").content(content).contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().is4xxClientError());
        }

        {
            String content = testHelper.loadResource("classpath:updateTicket_valid_1.json");
            mockMvc.perform(patch("/tickets/1").content(content).contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().is4xxClientError());

            content = testHelper.loadResource("classpath:updateTicket_valid_2.json");
            mockMvc.perform(patch("/tickets/1").content(content).contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().is4xxClientError());
        }

        for (String content : testHelper.loadResources("classpath:addAccount_valid*.json")) {
            mockMvc.perform(post("/accounts").content(content).contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().is2xxSuccessful());
        }

        {
            String content = testHelper.loadResource("classpath:updateTicket_valid_1.json");
            mockMvc.perform(patch("/tickets/1").content(content).contentType(MediaType.APPLICATION_JSON))
                    .andDo(print())
                    .andExpect(status().is2xxSuccessful())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(jsonPath("comments").isArray())
                    .andExpect(jsonPath("comments[0].id").value(1))
                    .andExpect(jsonPath("comments[0].text").value("assign to support agent"))
                    .andExpect(jsonPath("comments[0].commenter.id").value(2))
                    .andExpect(jsonPath("comments[0].commenter.name").value("test support account"))
                    .andExpect(jsonPath("comments[0].commenter.email").value("agent@example.com"))
                    .andExpect(jsonPath("comments[0].commenter.role").value("SUPPORT_AGENT"));

            content = testHelper.loadResource("classpath:updateTicket_valid_2.json");
            mockMvc.perform(patch("/tickets/1").content(content).contentType(MediaType.APPLICATION_JSON))
                    .andDo(print())
                    .andExpect(status().is2xxSuccessful())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(jsonPath("comments").isArray())
                    .andExpect(jsonPath("comments[1].id").value(2))
                    .andExpect(jsonPath("comments[1].text").value("new comment from customer"))
                    .andExpect(jsonPath("comments[1].commenter.id").value(1))
                    .andExpect(jsonPath("comments[1].commenter.name").value("test customer account"))
                    .andExpect(jsonPath("comments[1].commenter.email").value("asd@customer.com"))
                    .andExpect(jsonPath("comments[1].commenter.role").value("CUSTOMER_ACCOUNT"));
        }
    }

    @Test
    @Order(5)
    public void getTicketsTest2() throws Exception {
        mockMvc.perform(get("/tickets"))
                .andDo(print())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("[0].comments", hasSize(2)))
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    @Order((6))
    public void getCustomerTicketsTest() throws Exception {
        mockMvc.perform(get("/tickets/customer/1"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)));

        mockMvc.perform(get("/tickets/customer/2"))
                .andExpect(status().is4xxClientError());

        String content = testHelper.loadResource("classpath:addCustomer_valid_2.json");
        mockMvc.perform(post("/customers").content(content).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is2xxSuccessful());

        mockMvc.perform(get("/tickets/customer/2"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(0)));
    }

    @Test
    @Order(7)
    public void deleteTicketTest() throws Exception {
        mockMvc.perform(delete("/tickets/1"))
                .andExpect(status().is2xxSuccessful());

        mockMvc.perform(delete("/tickets/1"))
                .andExpect(status().is4xxClientError());
    }
}
