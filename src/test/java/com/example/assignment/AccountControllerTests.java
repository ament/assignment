package com.example.assignment;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
class AccountControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private TestHelper testHelper;

    @Test
    @Order(1)
    public void getAccountsTest() throws Exception {
        mockMvc.perform(get("/accounts"))
                .andDo(print())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("$").isArray());
    }

    @Test
    @Order(2)
    public void getAccountTest() throws Exception {
        mockMvc.perform(get("/accounts/1"))
                .andDo(print())
                .andExpect(status().is4xxClientError());
    }

    @Test
    @Order(3)
    public void addAccountTest() throws Exception {
        for (String content : testHelper.loadResources("classpath:addAccount_invalid*.json")) {
            mockMvc.perform(post("/accounts").content(content).contentType(MediaType.APPLICATION_JSON))
                    .andDo(print())
                    .andExpect(status().is4xxClientError());
        }

        String content = testHelper.loadResource("classpath:addAccount_valid_1.json");
        mockMvc.perform(post("/accounts").content(content).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("id").value(1))
                .andExpect(jsonPath("name").value("test customer account"))
                .andExpect(jsonPath("email").value("asd@customer.com"))
                .andExpect(jsonPath("role").value("CUSTOMER_ACCOUNT"));

        content = testHelper.loadResource("classpath:addAccount_valid_2.json");
        mockMvc.perform(post("/accounts").content(content).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("id").value(2))
                .andExpect(jsonPath("name").value("test support account"))
                .andExpect(jsonPath("email").value("agent@example.com"))
                .andExpect(jsonPath("role").value("SUPPORT_AGENT"));
    }

    @Test
    @Order(4)
    public void getAccountTest2() throws Exception {
        mockMvc.perform(get("/accounts/1"))
                .andDo(print())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    @Order(5)
    public void getAccountsTest2() throws Exception {
        mockMvc.perform(get("/accounts"))
                .andDo(print())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("$", hasSize(2)));
    }
}
