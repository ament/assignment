package com.example.assignment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

@Component
public class TestHelper {
    @Autowired
    ResourcePatternResolver resolver;

    public String loadResource(String location) throws IOException {
        Resource resource = resolver.getResource(location);
        return Files.readString(resource.getFile().toPath());
    }

    public List<String> loadResources(String location) throws IOException {
        Resource[] resources = resolver.getResources(location);

        List<String> contents = new ArrayList<>();
        for (Resource resource : resources) {
            contents.add(Files.readString(resource.getFile().toPath()));
        }

        return contents;
    }
}
