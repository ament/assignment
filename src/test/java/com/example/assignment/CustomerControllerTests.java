package com.example.assignment;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
class CustomerControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private TestHelper testHelper;

    @Test
    @Order(1)
    public void getCustomersTest() throws Exception {
        mockMvc.perform(get("/customers"))
                .andDo(print())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("$").isArray());
    }

    @Test
    @Order(2)
    public void getCustomerTest() throws Exception {
        mockMvc.perform(get("/customers/1"))
                .andDo(print())
                .andExpect(status().is4xxClientError());
    }

    @Test
    @Order(3)
    public void addCustomerTest() throws Exception {
        String content = testHelper.loadResource("classpath:addCustomer_invalid.json");
        mockMvc.perform(post("/customers").content(content).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is4xxClientError());

        content = testHelper.loadResource("classpath:addCustomer_valid_1.json");
        mockMvc.perform(post("/customers").content(content).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("id").value(1))
                .andExpect(jsonPath("name").value("test AB"));

        content = testHelper.loadResource("classpath:addCustomer_valid_1.json");
        mockMvc.perform(post("/customers").content(content).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is4xxClientError());
    }

    @Test
    @Order(4)
    public void getCustomerTest2() throws Exception {
        mockMvc.perform(get("/customers/1"))
                .andDo(print())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("$").isMap());
    }

    @Test
    @Order(5)
    public void deleteCustomerTest() throws Exception {
        mockMvc.perform(delete("/customers/1"))
                .andDo(print())
                .andExpect(status().is2xxSuccessful());

        mockMvc.perform(delete("/customers/1"))
                .andDo(print())
                .andExpect(status().is4xxClientError());
    }
}
