package com.example.assignment.account;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class AccountController {

    private final AccountRepository accountRepository;

    public AccountController(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @GetMapping("/accounts")
    public List<Account> getAccounts() {
        return accountRepository.findAll();
    }

    @GetMapping("/accounts/{id}")
    public ResponseEntity<Account> getAccount(@PathVariable Long id) {
        Optional<Account> account = accountRepository.findById(id);

        if (account.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(account.get());
    }

    @PostMapping("/accounts")
    public ResponseEntity<Account> addAccount(@RequestBody Account account) {
        if (StringUtils.isEmpty(account.getName()) ||
            StringUtils.isEmpty(account.getEmail())) {
            return ResponseEntity.badRequest().build();
        }

        // Add default role
        if (account.getRole() == null) {
            account.setRole(Role.CUSTOMER_ACCOUNT);
        }

        return ResponseEntity.ok(accountRepository.save(account));
    }
}
