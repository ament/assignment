package com.example.assignment.account;

public enum Role {
    CUSTOMER_ACCOUNT,
    SUPPORT_AGENT
}
