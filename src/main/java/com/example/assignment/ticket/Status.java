package com.example.assignment.ticket;

public enum Status {
    IN_QUEUE,
    IN_PROGRESS,
    RESOLVED
}
