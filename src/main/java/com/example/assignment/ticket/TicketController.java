package com.example.assignment.ticket;

import com.example.assignment.account.Account;
import com.example.assignment.account.AccountRepository;
import com.example.assignment.comment.Comment;
import com.example.assignment.comment.CommentRepository;
import com.example.assignment.customer.Customer;
import com.example.assignment.customer.CustomerRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class TicketController {

    private final TicketRepository ticketRepository;

    private final CustomerRepository customerRepository;

    private final CommentRepository commentRepository;

    private final AccountRepository accountRepository;

    @Autowired
    public TicketController(TicketRepository ticketRepository, CustomerRepository customerRepository, CommentRepository commentRepository, AccountRepository accountRepository) {
        this.ticketRepository = ticketRepository;
        this.customerRepository = customerRepository;
        this.commentRepository = commentRepository;
        this.accountRepository = accountRepository;
    }

    @GetMapping("/tickets")
    public List<Ticket> getTickets() {
        List<Ticket> tickets = ticketRepository.findAll();

        // Add comments
        tickets.forEach(ticket -> {
            List<Comment> comments = commentRepository.findByTicketId(ticket.getId());
            ticket.setComments(comments);
        });

        return tickets;
    }

    @GetMapping("/tickets/customer/{id}")
    public ResponseEntity<List<Ticket>> getCustomerTickets(@PathVariable Long id) {
        if (!customerRepository.existsById(id)) {
            return ResponseEntity.badRequest().build();
        }

        // Find all customer's tickets and add comments
        List<Ticket> tickets = ticketRepository.findByCustomerId(id);
        tickets.forEach(ticket -> {
            List<Comment> comments = commentRepository.findByTicketId(ticket.getId());
            ticket.setComments(comments);
        });

        return ResponseEntity.ok(tickets);
    }

    @GetMapping("/tickets/{id}")
    public ResponseEntity<Ticket> getTicket(@PathVariable Long id) {
        Optional<Ticket> ticketOptional = ticketRepository.findById(id);
        if (ticketOptional.isEmpty()) {
            return ResponseEntity.badRequest().build();
        }
        Ticket ticket = ticketOptional.get();

        List<Comment> comments = commentRepository.findByTicketId(id);
        ticket.setComments(comments);

        return ResponseEntity.ok(ticket);
    }

    @PostMapping("/tickets")
    public ResponseEntity<Ticket> createTicket(@RequestBody Ticket ticket) {
        if (StringUtils.isBlank(ticket.getTitle()) ||
            StringUtils.isBlank(ticket.getDescription()) ||
            ticket.getCustomer() == null ||
            StringUtils.isBlank(ticket.getCustomer().getName())) {
            return ResponseEntity.badRequest().build();
        }

        List<Customer> customers = customerRepository.findByName(ticket.getCustomer().getName());
        if (customers.isEmpty()) {
            return ResponseEntity.badRequest().build();
        }

        ticket.setCustomer(customers.get(0));
        ticket.setStatus(Status.IN_QUEUE);

        return ResponseEntity.ok(ticketRepository.save(ticket));
    }

    @PatchMapping("/tickets/{id}")
    public ResponseEntity<Ticket> updateTicket(@RequestBody TicketUpdate update, @PathVariable Long id) {
        if (StringUtils.isEmpty(update.comment)) {
            return ResponseEntity.badRequest().build();
        }

        // Commenter account or assignee account is required
        if (update.commenter == null && update.assignee == null) {
            ResponseEntity.badRequest().build();
        }

        // Commenter account and assignee account cannot be set at the same time
        if (update.commenter != null && update.assignee != null) {
            ResponseEntity.badRequest().build();
        }

        // Check if the ticket exists
        Optional<Ticket> ticketOptional = ticketRepository.findById(id);
        if (ticketOptional.isEmpty()) {
            return ResponseEntity.badRequest().build();
        }
        Ticket ticket = ticketOptional.get();

        Account commenter = null;

        // Assign ticket to support agent and change status
        if (update.assignee != null && update.status != null) {
            List<Account> accounts = accountRepository.findByEmail(update.assignee.getEmail());
            if (accounts.isEmpty()) {
                return ResponseEntity.badRequest().build();
            }
            commenter = accounts.get(0);

            if (update.status == Status.IN_PROGRESS) {
                ticket.setAssignee(commenter);
                ticket.setStatus(update.status);
            }
        }
        // Add new comment from customer account
        else if (update.commenter != null) {
            List<Account> accounts = accountRepository.findByEmail(update.commenter.getEmail());
            if (accounts.isEmpty()) {
                return ResponseEntity.badRequest().build();
            }
            commenter = accounts.get(0);
        }

        // Save comment and ticket updates
        Comment comment = new Comment();
        comment.setTicket(ticket);
        comment.setText(update.comment);
        comment.setCommenter(commenter);
        commentRepository.save(comment);

        ticket.getComments().add(comment);
        ticketRepository.save(ticket);

        return ResponseEntity.ok(ticket);
    }

    @DeleteMapping("/tickets/{id}")
    public ResponseEntity<Void> deleteTicket(@PathVariable Long id) {
        Optional<Ticket> ticketOptional = ticketRepository.findById(id);
        if (ticketOptional.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        Ticket ticket = ticketOptional.get();
        commentRepository.deleteAll(ticket.getComments());

        ticketRepository.deleteById(id);
        return ResponseEntity.ok().build();
    }
}
