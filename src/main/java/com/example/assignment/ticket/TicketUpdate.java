package com.example.assignment.ticket;

import com.example.assignment.account.Account;

public class TicketUpdate {

    public Account assignee;

    public Account commenter;

    public String comment;

    public Status status;
}
