CREATE TABLE ACCOUNT (ID IDENTITY NOT NULL, EMAIL VARCHAR(255), NAME VARCHAR(255), ROLE INTEGER, PRIMARY KEY (ID))
CREATE TABLE COMMENT (ID IDENTITY NOT NULL, TEXT VARCHAR(255), COMMENTER_ID BIGINT, TICKET_ID BIGINT, PRIMARY KEY (ID))
CREATE TABLE CUSTOMER (ID IDENTITY NOT NULL, NAME VARCHAR(255), PRIMARY KEY (ID))
CREATE TABLE TICKET (ID IDENTITY NOT NULL, DESCRIPTION VARCHAR(255), STATUS INTEGER, TITLE VARCHAR(255), ASSIGNEE_ID BIGINT, CUSTOMER_ID BIGINT, PRIMARY KEY (ID))

ALTER TABLE COMMENT ADD CONSTRAINT COMMENT_COMMENTER_ID_CONSTRAINT FOREIGN KEY (COMMENTER_ID) REFERENCES ACCOUNT
ALTER TABLE COMMENT ADD CONSTRAINT COMMENT_TICKET_ID_CONSTRAINT FOREIGN KEY (TICKET_ID) REFERENCES TICKET
ALTER TABLE TICKET ADD CONSTRAINT TICKET_ASSIGNEE_ID_CONSTRAINT FOREIGN KEY (ASSIGNEE_ID) REFERENCES ACCOUNT
ALTER TABLE TICKET ADD CONSTRAINT TICKET_CUSTOMER_ID_CONSTRAINT FOREIGN KEY (CUSTOMER_ID) REFERENCES CUSTOMER