# README

## Project layout

| File / directory                     | Description                                                              |
|--------------------------------------|--------------------------------------------------------------------------|
| src/test/java.com/example/assignment | All tests for the project                                                |
| src/test/coverage                    | Test code coverage report                                                |
| src/test/resources                   | Test data for all tests                                                  |
| src/main/resources                   | SQL schema and application configuration                                 |
| src/main/java/com/example/assignment | Project source files                                                     |
| Dockerfile                           | Dockerfile in root, uses alpine image with OpenJDK 17 exposing port 8090 |